﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float Speed;
    public float JumpHeigth;

    Rigidbody2D rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float movement = Input.GetAxis("Horizontal");
        transform.Translate(Vector2.right * movement * Time.deltaTime * Speed);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidbody.AddRelativeForce(Vector2.up * JumpHeigth, ForceMode2D.Impulse);
        }
    }

    public void SetRespawnPoint(Transform respawnPossition)
    {
        Debug.Log("Checkpoint");
    }
}
