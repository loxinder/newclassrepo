﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Checkpoint : MonoBehaviour
{
    public bool EndGametrigger;
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (EndGametrigger)
            {
                SceneManager.LoadScene("mainmanu");
            }
            else
            {
                other.GetComponent<Player>().SetRespawnPoint(transform);
            }
        }

    }

}
